Debian installation mentorsdojo.com (hackathon starter)

Mariadb: 


https://downloads.mariadb.org/mariadb/repositories/#mirror=tuna&distro=Debian&distro_release=wheezy--wheezy&version=10.0




Mailgun Driver:


To use the Mailgun driver, first install Guzzle, then set the driver option in your `config/mail.php` configuration file to mailgun. 

Next, verify that your `config/services.php` configuration file contains the following options:

'mailgun' => [
    'domain' => 'your-mailgun-domain',
    'secret' => 'your-mailgun-key',
],
