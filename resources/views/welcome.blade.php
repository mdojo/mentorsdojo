@extends('layouts.master')

@section('content')
    <div class="main-container">
         <h1>Learn from mentors</h1>
         <p class="lead"><a target=_blank href="https://www.urbandictionary.com/define.php?term=flearn">Flearn</a> from experienced mentors.</p>

         <hr>

         <div class="row">
            <div class="col-sm-6">
                <h2>Open Learning Paths</h2>

                <p>Mentors can post Open Learning Paths - which are a set of learning modules based on their experience of learning.</p>
                <p><a href="#" role="button" class="btn btn-default">View details »</a></p>
            </div>
            <div class="col-sm-6">
                <h2>Ask questions</h2>
                <p>Post a question, get answers from experts.</p>
                <p><a href="#" role="button" class="btn btn-default">View details »</a></p>
            </div>


            <div class="col-sm-12">
                <h2>Have fun</h2>
                <p>Minim esse vinyl, flexitarian id kombucha street art ramps. Irony est accusamus excepteur, intelligentsia laborum veniam echo park. Cray brunch mumblecore, neutra enim venmo hella voluptate mlkshk deep v single-origin coffee portland chillwave vice trust fund. Ad kombucha kale chips meggings. You probably haven't heard of them hella nulla echo park. Ad sunt excepteur vero trust fund, truffaut keffiyeh cardigan roof party farm-to-table celiac lumbersexual sriracha. Fanny pack pitchfork aute cold-pressed, readymade nisi distillery freegan sartorial direct trade PBR&B XOXO nostrud knausgaard next level.</p>
                <p><a href="#" role="button" class="btn btn-default">View details »</a></p>
            </div>



        </div>
    </div>
@stop
