@extends('layouts.master')

@section('content')
    <div class="main-container">
        <h2>API Examples</h2>

        <hr/>

        <div class="row">
		    <div class="col-sm-4">
			<a href="{{route('api.facebook')}}" style="color: #fff">
			    <div style="background-color: #3b5998" class="panel panel-default">
				<div class="panel-body">
				    <img src="http://i.imgur.com/jiztYCH.png" height="40"> Facebook
				</div>
			    </div>
			</a>
		    </div>
			<div class="col-sm-4">
			<a href="{{route('api.github')}}" style="color: #fff">
			    <div style="background-color: #000" class="panel panel-default">
				<div class="panel-body">
				    <img src="http://i.imgur.com/2AaBlpf.png" height="40">GitHub
				</div>
			    </div>
			</a>
		    </div>
	    </div>
    </div>
@stop
