
--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` text NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(80) NOT NULL,
  `email` text NOT NULL,
  `user_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `fullname`, `username`, `password`, `email`, `user_type`) VALUES
(7, 'Jose Palala', 'josepalala', '33806635422c22ea2d64ab768454f6f4', 'deterium73@gmail.com', 1),
(11, 'Glenn', 'glenn', '313d99bb1c4738b9fa392f8cb2ca9779', 'gsantos@gmail.com', 2),
(13, '', 'JohnLouieBiÃ±as', 'c6b86b6566846e56c9c9ef17b40178dd', '', 2),
(15, 'John Louie Binas', 'imjohnlouie', '75a154014cb1fcb2c360ae4edaef2c05', 'akoaysilouie@yahoo.com', 1),
(17, 'David Marquez', 'dayvough', '0b6f0ab0f4f8de75b86d448534dab5f1', 'david.marquez@me.com', 1),
(19, 'Jason Torres', 'jason', '42acf0f2a51102e170799c5f68976803', 'jason@proudcloud.net', 2),
(20, 'Norbert Aquende', 'norbs888', '15cac440f71a45c50a2d4010823a6424', 'norbert@aquende.com', 2),
(22, 'Jay Fajardo', 'jayfajardo', 'be888ba21111a8463f46055128c940ed', 'jmrfajardo@gmail.com', 2),
(23, 'Samuel Pigott', 'pigosd03', '7e204ab8fd1ba37f1c1127f35dba94c9', 'samuelpigott@gmail.com', 2),
(24, 'Miguel Paraz', 'mparaz', '6ae4f2faac99f0aec3d06e811d583473', 'mparaz@gmail.com', 2),
(29, 'John Louie Binas', 'imjohnlouie', '75a154014cb1fcb2c360ae4edaef2c05', 'akoaysilouie@yahoo.com', 1),
(31, 'Reg Ramos', 'regram', '8c3645f6e3cb41a2f09c24ec11551fcd', 'jra.ramos@gmail.com', 1),
(37, 'David Marquez', 'dayvoughh', '0b6f0ab0f4f8de75b86d448534dab5f1', 'david.marquez@me.com', 1),
(42, 'Takuya Oka', 'okatak', 'efe998a372d8d1917be45e066d97600f', 'takuya.oka@gmail.com', 1),
(43, 'alan landa', 'alanzky', '0b6f0ab0f4f8de75b86d448534dab5f1', 'alan.landa@gmail.com', 1),
(44, 'Maui', 'millanasia', 'a2a2419c2ad1da4599b8147f4f83c9c3', 'maui.millan@gmail.com', 1),
(45, 'Christian', 'christianbesler', 'cefc862b3d56b7961e179fa6f4d2d791', 'christian.besler@gmail.com', 1),
(46, 'Ryan', 'brainv', 'e22bfa6561627d08a655c704a8d2d887', 'ryan.escarez@gmail.com', 1),
(47, 'Israel Brizuela', 'sael', 'fc9471b71efac937b1a38bb5bf7abfcc', 'israel.brizuela@gmail.com', 1),
(48, 'Charo Nuguid', 'rcdiugun', 'f911db15ddbdd4bbf9eb4c559cd568ba', 'me@thegeekettespeaketh.com', 2),
(49, 'Jonathan Lansangan', 'Jonathan', '67c93cb854af6e64bbb8df63516633f8', 'jonathan@dynamicobjx.com', 2),
(50, 'Rafaeloca', 'Rafaeloca', 'bb85e1e780a2831da28a0fa7f619d5d8', 'rafael.dr.oca@gmail.com', 1),
(51, 'Jon Edward Santillan', 'jonsantillan', 'e0ae423966a776a693be29dc773e4470', 'santillanjonedward@gmail.com', 2),
(52, 'Ian Bert Tusil', 'iyanski', 'c064bbea461f3cfa52b98d004657f871', 'iyanski@gmail.com', 1),
(53, 'Karlo Licudine', 'pinoyblogero', '085ad51636b667579dde03249b3a98f3', 'karlo@accidentalrebel.com', 1),
(54, 'Andre Marcelo-Tanner', 'kzap', '6746a345cdcc2dc38f12c15e79d596fd', 'andre@enthropia.com', 2),
(55, 'ALVIN JOHN FERIAS', 'alvinjohn', 'd743187ca7dfa1f91b827091f31c1ccd', 'alvinjohnf@gmail.com', 1),
(56, '', 'gkway197', 'c6b86b6566846e56c9c9ef17b40178dd', '', 1),
(57, 'Glenn Santos', 'gsantos', 'ff7bf1a102498dba681aae606f8dd29a', 'glenn@mentorsdojo.com', 1),
(58, 'Wally W', 'wally', 'f975287223ad20d68dffddf3ad5f7a0a', 'wallyw@wally.wal', 2),
(59, 'Ramon Pastor', 'rcpastor', '36bc318bed76a0dccc2dc86ee60f73ce', 'rcpastor@numlock.com.ph', 2),
(60, 'RJ David', 'rjrdavid', '8e97c81e845cefe7656d20636f1de397', 'rjrdavid@gmail.com', 2),
(61, 'Shifu', 'mastershifu', '91374fbf5a480ee946f6e52aa3629567', 'shifu@mentorsdojo.com', 2),
(62, 'Peter Paul Cauton', 'pcauton', '71426ea1f56efa3924902704dc724398', 'pcauton@yahoo.com', 2),
(63, '', 'asd', '518bd651ae368cd50d116b5b75c36e17', 'asdasd', 2),
(64, 'mio galang', 'mio', '1274eeb1ef7eff0d9c6a46f5084e402a', 'miguelgalang@gmail.com', 1),
(65, 'Glenn Yahoo', 'glennyahoo', 'cdcb2e32e74d79401d5a7d405eb35166', 'garilenard@yahoo.com', 1),
(66, 'Glenn Santos', 'glennsantos', 'f8b2d832f596e52e99b6ffd72160e670', 'glenn@memokitchen.com', 1),
(67, 'Tester', 'tester', '313d99bb1c4738b9fa392f8cb2ca9779', 'email@email.com', 1),
(68, 'Ari Bancale', 'GameMaster', 'fe434e249758148b52b005fc3d15b511', 'aribancale@gmail.com', 1),
(69, 'tester', 'tester2', 'fddf0bdbf2ecf74aee8a4e8a3c112ec8', 'tester@tester.com', 1),
(70, 'Christian Blanquera', 'cblanquera', '616231faa92cce2e4a755745c84630e2', 'cblanquera@gmail.com', 1),
(71, 'Guyi Shen', 'guyi', 'd1028f816a9c76f78f4bcd2e0855ba32', 'guyi@lobangclub.com', 2),
(72, 'Ann Jacobe', 'annjacobe', 'e4bfc45a5f604516c7b87d42f751e281', 'avjacobe@yahoo.com', 2),
(73, 'Alvin Gendrano', 'agendrano', '945ae14828c895591728ae41cac896c3', 'alving@microsoft.com', 2),
(74, 'Terence Pua', 'terpua', 'bf7e05c446af9c96fd3ab41ee86bd793', 't@insynchq.com', 2),
(75, 'Art Ilano', 'artilano', '8fb16960f2b6eeeb445280ec6f9bd6b1', 'artilano@gmail.com', 2),
(76, 'Joe Palala', 'joe', '11b2b8b2099906d94fcd4caaca4c3b6d', 'deterium73@gmail.com', 1),
(77, 'Ben Adrian Sarmiento', 'bensarmiento', '7295bf8077adb7172c38b4b251989800', 'benadriansarmiento@gmail.com', 1),
(79, 'Jesse Panganiban', 'jpanganiban', 'a26a64302c36f723bffc83b3c51fc058', 'me@jpanganiban.com', 1),
(83, 'Rachel Jaro', 'rjaro', '7a9b4ae1c331ad2538b82fe05acd7937', 'rachel.jaro@gmail.com', 1),
(84, 'Carlson Orozco', 'carlsonorozco', 'aa94dd3802623f8051e5eb577530a094', 'carlsonorozco@yahoo.com', 1),
(85, 'Richard Grimaldo', 'chardinet', '1bdc6509229a90f8af015043d273b719', 'chard@intelstar.net', 1),
(86, 'John Louie BiÃ±as', 'akoaysilouie', '75a154014cb1fcb2c360ae4edaef2c05', 'akoaysilouie@yahoo.com', 1),
(87, 'Test Orozco', '  ', '85a0f8258661e1215a82f8647c0444c2', 'carlsonorozco@yahoo.com', 1),
(88, 'jetro barrera', 'jetro', 'd3a6129f2724d9c6e4296d6c8548ebed', 'jetro2k5@yahoo.com', 1),
(89, 'Patricia Anne Alvarez Bayona', 'Patricia', '2e6b4f7278a7cf584c0f502edc4389df', 'patricia.bayona@uap.asia', 1),
(90, 'Jerome', 'jc26', 'f29dec65f10907e7a9a199ffb592000a', 'jerome.caisip@gmail.com', 1),
(91, 'exesteKayague', 'exesteKayague', '7c3b99f7a177af0c3335ff530924b973', 'allecusigue@gmail.com', 1),
(92, 'Albert Mercado', 'Albert', 'd3ea1a36108421fa8035fb6fc58aa3b2', 'a_luis_mercado@yahoo.com', 1),
(93, 'Mae Isabelle Turiana', 'maeisabelle', 'd75a7a774cdd1dec6472c2ad25370f4c', 'maeisabelle@gmail.com', 1),
(94, 'mmscsbqmcq', 'mmscsbqmcq', '6d1085c1e4b2d630db8a7ef185bbb621', 'xezxxapurn@qejyez.com', 1),
(95, 'nyfoywswzh', 'nyfoywswzh', '296e698792d4578022f72a57dab2f347', 'iegtbfikbu@ximaok.com', 1),
(96, 'uqawmpyntc', 'uqawmpyntc', '54a4bd4f82def6403268c0f59931f12e', 'rebhukrcnw@mnnujs.com', 1),
(97, 'jqbwgrioyp', 'jqbwgrioyp', 'f6763ed9d2968bd2f00a32cf153a41bf', 'emdyrjjgiu@cfzhwx.com', 1),
(98, 'bnntphzimh', 'bnntphzimh', '01e206fb4c8350e4981f401249ee49b9', 'vuagxbqite@lisjnc.com', 1),
(99, 'ezcxockwtl', 'ezcxockwtl', 'b0d63b7e9256c0f1087bc7e0b53bd33a', 'wkdkjamezy@rziswz.com', 1),
(100, 'josepalala', 'tester@test.com', '33806635422c22ea2d64ab768454f6f4', 'jose.palala@gmail.com', 1),
(101, 'deterium73', 'johndoe', '33806635422c22ea2d64ab768454f6f4', 'deterium73@gmail.com', 2),
(102, '123', 'testee123123', '33806635422c22ea2d64ab768454f6f4', 'ftawft', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member_details`
--

CREATE TABLE IF NOT EXISTS `member_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) NOT NULL,
  `startup` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about_startup` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schedule` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_expertise`
--

CREATE TABLE IF NOT EXISTS `member_expertise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expertise` varchar(254) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `member_expertise`
--

INSERT INTO `member_expertise` (`id`, `expertise`, `active`) VALUES
(1, 'Business Development', 1),
(2, 'Creative', 1),
(3, 'Finance', 1),
(4, 'Human Resources', 1),
(5, 'Management', 1),
(6, 'Sales and Marketing', 1),
(7, 'Software Development ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member_img`
--

CREATE TABLE IF NOT EXISTS `member_img` (
  `img_url` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `member_profiles`
--

CREATE TABLE IF NOT EXISTS `member_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) NOT NULL,
  `fullname` text NOT NULL,
  `about` text NOT NULL,
  `expertise` text NOT NULL,
  `industries` text NOT NULL,
  `linkedin` text NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `startup_name` text NOT NULL,
  `about_startup` text NOT NULL,
  `need_help` text,
  `providing` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `member_profiles`
--

INSERT INTO `member_profiles` (`id`, `mem_id`, `fullname`, `about`, `expertise`, `industries`, `linkedin`, `facebook`, `twitter`, `startup_name`, `about_startup`, `need_help`, `providing`) VALUES
(2, 19, 'Jason Torres', '<div>Development Lead / CTO,&nbsp;<a href="http://proudcloud.net" title="" target="_blank">Proudcloud</a></div><div><br></div><div>We''re a new kind of web engineering firm with a relentless passion for innovation. We strive to share this passion with our clients to create a mutual desire to build great things.<br></div><div><br></div><div>Our projects include:<br></div><div><br></div><div><a href="http://www.artisteconnect.com" title="" target="_blank">Artisteconnect</a><br></div><div>ArtisteConnect is the most awesome way to support and get connected with local artists you love. Past, Present and Future.<br></div><div><br></div><div><a href="http://www.nowshowing.ph" title="" target="_blank">NowShowing</a><br></div><div>Discover new movies, grab the perfect schedule, and stay updated with upcoming titles.&nbsp;Also available in the <a href="http://itunes.apple.com/ph/app/now-showing/id483134984?mt=8" title="" target="_blank">App Store</a>.</div><div><br></div><div><a href="http://socialcheck.me" title="" target="_blank">Socialcheck.me</a>,<br></div><div>Make Better Hiring Decisions<br></div><div><br></div><div><a href="http://caterconcierge.com" title="" target="_blank">Caterconcierge</a><br></div><div>Corporate catering made easy</div><div><br></div><div><a href="http://yiiip.com" title="" target="_blank">Yiiip</a><br></div><div>Buy &amp; sell on the go</div><div><br></div><div><a href="http://www.swap.ph" title="" target="_blank">Swap.ph</a><br></div><div>and more!</div><div><br></div><div><div>Mentoring Hours:&nbsp;<br></div><div>Preferred Meeting Place: Proudcloud Office</div></div>', 'Software Development ', 'Startups, Ruby on Rails, Mobile Development, Social Computing', 'jasonetorres', 'jason.e.torres', 'jasontorres', '', '', '', '<span style="text-align: justify; ">I can mentor on the following topics&nbsp;</span><div><ol><li><span style="text-align: justify; ">Ruby</span><br></li><li><span style="text-align: justify; ">Ruby on Rails</span><br></li><li><span style="text-align: justify; ">Sinatra</span></li><li><span style="text-align: justify; ">MySQL</span></li><li><span style="text-align: justify; ">PostgreSQL</span></li><li><span style="text-align: justify; ">jQuery</span></li><li><span style="text-align: justify; ">Javascript</span></li><li><span style="text-align: justify; ">CSS</span></li><li><span style="text-align: justify; ">HTML</span></li><li><span style="text-align: justify; ">Linux</span></li><li><span style="text-align: justify; ">Servers</span></li><li><span style="text-align: justify; ">MongoDB</span></li><li><span style="text-align: justify; ">NodeJS</span></li><li><span style="text-align: justify; ">Ubuntu/Debian</span></li><li><span style="text-align: justify; ">CentOS</span></li><li><span style="text-align: justify; ">Cloud Computing</span></li><li><span style="text-align: justify; ">Amazon AWS</span></li><li><span style="text-align: justify; ">Ruby on Rails Philippines</span></li><li><span style="text-align: justify; ">Lead Engineering</span></li></ol><span style="text-align: justify; ">Quora: </span><a class="linkclass" href="http://www.quora.com/Jason-Torres" style="text-align: justify; ">http://www.quora.com/Jason-Torres</a><br></div>'),
(3, 20, 'Norbert Aquende', 'I am a certified mobile marketing professional, blue ocean strategy practicioner and Windows Phone expert.&nbsp;<div>Business development professional with IT project management experience; extensive product management ability, project development and management skills, holds vast technical knowledge needed to define and realize new technologies and products, in-depth knowledge in smart devices such as Windows Mobile, Windows Phone and Android.</div><div><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place:&nbsp;</div></div>\n\n\n\n\n\n\n\n', 'Business Development,Sales and Marketing', 'Telecoms, Mobile Marketing, Project Management, Unified Communications, Channels Management, Systems Design', 'norbs', 'facebook.com/aquende', 'norbs888', '', '', '', 'I can mentor on the following:<div><ol><li>Strategic Planning</li><li>Mobile Technology</li><li>Customer Relations</li><li>Competitive Analysis</li><li>Balanced Scorecard</li><li>Blue Ocean Strategy</li><li>ERP Product Management</li><li>Mobile Marketing</li><li>Channel Management</li><li>Supply Chain</li><li>Community Management</li><li>Unified Communications</li><li>Marketing Strategy</li><li>Process Engineering</li><li>Product Development</li><li>Social Media</li><li>Sales Management</li><li>Market Planning</li><li>Negotiation</li><li>Planning</li><li>Forecasting</li><li>Team Building</li><li>Mobile Applications</li><li>Business Planning</li><li>Social Media Marketing</li><li>Mobile Internet</li><li>Online Marketing</li><li>Business Intelligence</li><li>New Business Development</li><li>Pricing</li><li>Market Analysis</li></ol><div>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Norbert-Aquende">http://www.quora.com/Norbert-Aquende</a><br></div></div>'),
(5, 22, 'Jay Fajardo', '<div>Founder, <a href="http://proudcloud.net" title="" target="_blank">Proudcloud</a><br><div>PROUDCLOUD creates rich, socially aware applications for web and iOS.&nbsp;Ruby on Rails + iOS + Javascript</div><div><br></div></div><div>Lead Organizer, <a href="http://manila.startupweekend.org" title="" target="_blank">Startup Weekend Manila</a></div><div><div>No Talk. All Action. Launch a Startup in 54 hours.&nbsp;Startup Weekend is a global network of passionate leaders and entrepreneurs on a mission to inspire, educate, and empower individuals, teams and communities. Come share ideas, form teams, and launch startups.</div></div><div><br></div><div><div>Founder and Chief Conspirator,&nbsp;<a href="http://roofcamp.net" title="" target="_blank">Roofcamp</a></div><div>Roofcamp is a venue for future looking, maverick, and fearless individuals sharing and distilling ideas from concept to reality.<br></div></div><div><br></div><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place: Proudcloud Office</div>', 'Software Development ', 'Startups, Ruby on Rails, Mobile Development, Social Computing', 'jayfajardo', 'jayfajardo', 'jayfajardo', '', '', '', '<div>I can mentor on the following topics&nbsp;<br></div><div><br></div><div>1. Ruby on Rails</div><div>2. Startups</div><div>3. Mobile Applications</div><div>4. Social Computing</div><div>5. Agile</div><div>6. Web 2.0</div><div>7. Entrepreneur</div><div>8. jQuery</div><div>9. AJAX</div><div>10. Web Development</div><div>11. CSS</div><div>12. Ruby</div><div>13. MySQL</div><div>14. JavaScript</div><div>15. REST</div><div>16. Web Applications</div><div>17. Cloud Computing</div><div>18. HTML5</div><div>19. Linux</div><div>20. Git&nbsp;</div><div><br></div><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Jay-Fajardo">http://www.quora.com/Jay-Fajardo</a></span></div>'),
(6, 23, 'Samuel Pigott', '<div>COO / Founder,&nbsp;<a href="http://www.sourcepad.com/" title="" target="_blank">SourcePad</a><br></div><div><div>We build Web, iPhone, iPad, Android &amp; Facebook apps for 75+ entrepreneurs.&nbsp;A lot of experience with website usability.</div></div><div><br></div><div><div>"Sam is an entrepreneur based out of New York City, living in Asia. Prior to SourcePad, Sam co-founded Clear Insight Group - a meditation-based training company that worked with Fortune 500 companies, non-profits &amp; law firms to reduce stress in the workplace.</div><div><br></div><div>Sam''s previous work was in public relations &amp; technology at Harvard University, American Express &amp; Fidelity Investments. He has a BS in MIS from Wake Forest University."</div></div><div><br></div><div><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place: SourcePad Office</div></div>', 'Business Development,Management', 'Internet, Facebook Development, Usability', 'samuelpigott', '', 'sampigott', '', '', '', 'I can mentor on the following topics:<div><ol><li>Marketing Strategy</li><li>Facebook apps</li><li>Mobile Apps</li></ol><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Sam-Pigott">http://www.quora.com/Sam-Pigott</a></span></div></div>'),
(7, 24, 'Miguel Paraz', '<div>"Miguel is working on building solutions for telecommunications, mobile, Internet, the web, and apps. This is both as a developer and consultant: software architecture and build, program management, and standards development organizations.</div><div><br></div><div>He co-founded one of the first Internet Service Provider''s in the Philippines, IPhil Communications. This started operating in 1994. The partners sold this to PSINet, a publicly-listed US ISP, in 2000.</div><div><br></div><div>He has been using and programming on Linux since 1994.</div><div><br></div><div>He is interested in software in general - design and implementation, computer science and algorithms, and open source tools and libraries."<br><br></div><div><span>Personal Website :: <a class="linkclass" href="http://flavors.me/mparaz">http://flavors.me/mparaz</a></span><br></div><div><br></div><div><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place:&nbsp;</div></div>', 'Software Development ', 'Telecoms, Mobile, Software Development, Software Architecture', 'mparaz', 'mparaz', 'mparaz', '', '', '', '<div>I can mentor about:</div><div><br></div><div><ol><li>Mobile</li><li>Linux</li><li>Java</li><li>JavaScript</li><li>WAC</li><li>Android</li><li>Software architecture and development for Internet and mobile applications.</li></ol><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Miguel-Paraz">http://www.quora.com/Miguel-Paraz</a></span></div></div>'),
(12, 29, 'John Louie Binas', '<div><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;">Hello there! I''m John louie Binas, a freelance graphic designer from the Philppines.</span></font></div><div><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;"><br></span></font></div><div><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;">My goal is to help businesses connect with their costumers through helpful and user-friendly graphic content.</span></font></div><div><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;"><br></span></font></div><div><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;">I lend a hand with the following:</span></font></div><div><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;"><br></span></font></div><div><ul><li><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;">Icons</span></font></li><li><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;">Logos</span></font></li><li><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;">Business Card</span></font></li><li><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;">Infographics</span></font></li><li><font color="#58595b" face="Arial"><span style="font-size: 14px; line-height: 21px;">Web Design</span></font></li></ul></div>', '', 'Web Design, Logo Design, Infographics, ', '', 'johnlouie04', 'imjohnlouie', '', '', '<br>', ''),
(14, 31, 'Reg Ramos', 'Startup Wannabe', '', 'Telco', '', 'regagainsthemachine', 'dronthego', '', '', 'how to be a startup rockstar', ''),
(20, 37, 'David Marquez', 'Avid video gamer, student, Mac enthusiast.', '', 'Programming', '', 'ThundrShocK', '', '', '', 'Programming experience!', ''),
(25, 42, 'Takuya Oka', 'What''s up Glen', 'Business Development, ', 'Marketing Research', '', '', '', '', '', '', ''),
(27, 44, 'Maui', '', 'Business Development, ', 'Telecoms', '', 'www.facebook.com/maui.millan', '', '', '', 'advice on biz model', ''),
(29, 46, 'Ryan', '', 'Software Development , ', '', '', '', '', '', '', '', ''),
(30, 47, 'Israel Brizuela', 'cute', 'Management', 'Finance, ERP, SEO ', '', '', 'saelph', '', '', '<br>', ''),
(31, 48, 'Charo Nuguid', '<div>Trainer and Consultant experienced in project management, software engineering, enterprise web applications and application security.&nbsp;</div><div><br></div><div>Currently doing work in Java and Android technologies.&nbsp;</div><div><br></div><div>Interested in open source technology. Logistics Nazi.</div><div><br></div><div><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place:&nbsp;</div></div>', 'Software Development ', 'Android Development, Project Management, Enterprise Web Applications, Software Engineering', 'rosarionuguid', 'headgeekette', 'rcdiugun', '', '', '', 'I can mentor about:<div><ol><li>Android Development</li><li>Enterprise Web Applications<br></li><li>Software Engineering<br></li><li>Training</li><li>Community building<br></li></ol><div><span>Quora: <a class="linkclass" href="http://www.quora.com/Charo-Nuguid">http://www.quora.com/Charo-Nuguid</a></span><br></div></div>'),
(32, 49, 'Jonathan Lansangan', 'Footballer, photographer, traveler. People &amp; biz developer. Dad, dreamer, poet. Real Madrid &amp; Brazil fan. F1 enthusiast.&nbsp;<div><br></div><div>Plays badminton &amp; table tennis. Enjoys Jap &amp; Italian food, wine, ice cream, chocolates &amp; a starry night.&nbsp;</div><div><br></div><div>Dreams of driving a 4x4 around the whole Philippines. Will back-pack across Europe by 2018. Plans on seeing 15,025 sunsets beginning Jan. 8, 2012.&nbsp;</div><div><br></div><div>Resident non-geek, disturber of the peace, COO of <a href="http://dynamicobjx.com/" title="" target="_blank">Dynamic Objx</a> Labs, Inc., a Java-Scala-Lift-Play-RoR-Android-iOS software development greenhouse based in Manila, Philippines.<br></div><div><br></div><div><div>Mentoring Hours: Afternoons</div><div>Preferred Meeting Place: Dynamic Objx Office, BGC, Ortigas Center</div></div>', 'Business Development,Human Resources,Management,Sales and Marketing', '', '', 'profile.php?id=683184421', 'jelansangan', '', '', '', '<div>I can mentor in the following:</div><div><ol><li>Business Development</li><li>Product Development</li><li>Leadership &amp; Management</li><li>Project Management</li><li>Problem Solving &amp; Decision-Making</li><li>Creativity &amp; Innovation</li><li>Presentation Skills</li><li>Operational best practices across various industries</li></ol></div><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Jonathan-Earl-Lansangan">http://www.quora.com/Jonathan-Earl-Lansangan</a></span></div>'),
(33, 50, 'Rafaeloca', 'Lets innovate', 'Business Development', 'Technology, marketing', '', '', 'rafaeloca', '', '', '<br>', ''),
(35, 52, 'Ian Bert Tusil', 'Ruby on Rails Developer, Technopreneur, Technologist', 'Software Development , ', '', '', '', '', '', '', 'business development, etc.', ''),
(36, 53, 'Karlo Licudine', 'I am the developer on the Kiddle team which won first place at Startup Weekend Manila 2012.', 'Software Development , ', 'Web Development, Game Development, Software Development', 'http://ph.linkedin.com/in/karlolicudine', 'http://www.facebook.com/karlo.licudine', '', '', '', 'We want help in locating holes in our idea and business model so that we can find a solution and fix it.\n\nAlso, all team members are new to startups, a push in the right direction to lift our idea off the ground would be very helpful.', ''),
(37, 54, 'Andre Marcelo-Tanner', 'Creating stuff on the web since 1996.<div><br></div><div><div>President,&nbsp;<a href="http://www.enthropia.com.ph/" title="" target="_blank">Enthropia Inc.</a>&nbsp;</div></div><div><br></div><div><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place:&nbsp;</div></div>', 'Business Development,Finance,Management,Software Development ', 'Group Buying, Domains, Online Gaming, Web Programming, Server Management, Content Deployment, Search Engine Optimization.', 'andremarcelotanner', 'andre.marcelotanner', 'kzapkzap', '', '', '', '<div>I can mentor on the following:</div><div><ol><li>Web Programming</li><li>Server Management</li><li>Content Deployment</li><li>Search Engine Optimization.<br></li><li>Ideation</li><li>Software Development</li><li>Scaling Businesses</li></ol><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Andre-Jay-Marcelo-Tanner">http://www.quora.com/Andre-Jay-Marcelo-Tanner</a></span></div></div>'),
(38, 55, 'Alvin John Ferias', ' Alvin John Ferias a Pinoy website developer (proud to be a Filipino Programmer  ) , SEO webmaster, Joomla Website Developer and Filipino Website Designer base in Metro Manila Philippines. (Please click here to view my resume) webdeveloper.com.ph provides a full service â€“ from consultation and strategic planning to web development and search engine optimization â€“ together with long term management and support for your web project. I offers high quality web development to power your business career and other organization. Every great business idea takes wings with the launch of a professionally designed website. Internet has redefined the way business is transacted and your website is the gateway to these business opportunities.', 'Creative, Management, Software Development , ', 'Website Developer', 'http://www.linkedin.com/in/pinoywebdeveloper', 'https://www.facebook.com/webdeveloper.joomla.wordpress.seo.philipppines', '', '', '', 'Website Development, SEO, PHP', ''),
(43, 59, 'Ramon Pastor', '<div>Family man, unabashed geek, and iOS/Qt developer. Numlock head honcho</div><div><br></div><div>To be a leader in Application Development for iOS and other mobile platforms&nbsp;<br></div><div><br></div><div>Specialties</div><div><br></div><div>- Programming in Cocoa / Cocoa Touch / Objective-C / Qt</div><div>- Mobile Application Development</div><div><br></div><div>Founder, <a href="http://www.numlock.com.ph/pages/apps.php" title="" target="">Numlock Solutions</a></div><div><br></div><div>iOS Apps Published: WalletWhiz, MindMash, DeskWhiz, StreetSmart</div><div><br></div>\n<div>Mentoring Hours: 6 - 8pm, M-F</div><div>Preferred Meeting Place: Numlock Office</div><div>Fun Fact: I find your lack of faith disturbing.</div>', 'Management,Software Development ', 'iOS Development, Mac Development, Mobile Development', 'rcpastor', 'rcpastor', 'filjedi', '', '', '', '<div>I can mentor on the following topics&nbsp;</div><div><br></div><div>&nbsp;&nbsp;1. Objective-C</div><div>&nbsp; &nbsp;2. Cocoa</div><div>&nbsp; &nbsp;4. Mobile Applications</div><div>&nbsp; &nbsp;5. iPhone development</div><div>&nbsp; &nbsp;6. iPhone</div><div>&nbsp; &nbsp;7. Xcode</div><div>&nbsp; &nbsp;8. Application Development</div><div>&nbsp; &nbsp;9. Mobile</div><div>&nbsp; 10. Mobile Technology</div><div><br></div><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Ramon-Pastor">http://www.quora.com/Ramon-Pastor</a></span></div>'),
(44, 60, 'RJ David', 'Started <a href="http://Sulit.com.ph" title="" target="_blank">Sulit.com.ph</a> with his wife Arianne in September 2006 as a free classified ads website targeting the Philippines. Launched the website from his bedroom and it has grown to become the most visited local website in the country.&nbsp;<div><br></div><div>Prior to starting Sulit.com.ph, worked as a self-taught Web Developer and as a Software Test Engineer. Holds a Bachelor''s Degree in Mechanical Engineering from the University of the Philippines.&nbsp;<div><br></div><div>Mentoring Hours: 9am - 6pm, M-F<br></div></div><div>Preferred Meeting Place: Sulit Office</div>', 'Management,Software Development ', 'PHP Development, Ecommerce, SEO', '', 'rjdavid', '', '', '', '', '<div>I can mentor in:</div><div><br></div><ol><li>PHP Development</li><li>Ecommerce</li><li>SEO<br></li></ol><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/RJ-David">http://www.quora.com/RJ-David</a></span></div>'),
(46, 62, 'Peter Paul Cauton', '<div><span>I am a passionate advocate of the advancement of Filipino startups. The Juan Great Leap movement is a plea to spur more and more Filipinos to take the great startup leap. You can learn more at <a class="linkclass" href="http://www.juangreatleap.com">www.juangreatleap.com</a>&nbsp;I currently coach around a dozen entrepreneurs and would-be entrepreneurs, all amazing people.&nbsp;</span></div><div><br></div><div><span>STORM Consulting (<a class="linkclass" href="http://www.stormconsulting.com.ph">www.stormconsulting.com.ph</a>) is my baby and where I learned the startup ropes. It was the first firm I founded, completely bootstrapped, in 2006. Tired of the same old HR benefits in your firm? What STORM does is make benefit packages truly exciting again, through the provision of Flexible Benefits. I have seen this service change lives and change firms. Interested?</span></div><div><br></div><div>I am also the founder of Searchlight, Inc., a niched Senior Talent Acquisition firm. Our premise is simple: we are a firm which focuses on quality: we will only service ONE client per industry. Think about that for a minute.</div><div><br></div><div><div>Another new company I am involved in is a company called&nbsp;<a href="http://streamenginestudios.com/" title="" target="_blank">Streamengine Studios</a>. We believe that videos are just a better way in explaining things to people. We create online motion graphics videos which can help you explain concepts or processes much better.</div></div><div><br></div><div>We are currently incubating 4-5 other startups for launch this 2012, through our startup incubator, Bizkitchen.&nbsp;</div><div><br></div><div>I am very much a teacher at heart, and I derive thorough satisfaction in sharing knowledge and ideas through speaking engagements, training opportunities, and teaching.</div><div><br></div><div>I am a passionate advocate of entrepreneurship, harnessing talent, and reflecting God in the workplace.</div><div><br></div><div><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place: STORM Office, along Shaw Boulevard, Mandaluyong</div></div>', 'Business Development,Human Resources,Management,Sales and Marketing', 'Human Resources, Consulting, Entrepreneurship, Web services,  Internet Entrepreneur, Talent Acquisition, Recruitment', 'petercauton', 'peter.cauton', 'PeterCauton', '', '', '', 'I can mentor about:<div><br></div><div><ol><li>How to launch a startup</li><li>Assembling a Founding Team</li><li>The art of bootstrapping</li><li>Refining startup ideas</li><li>Lean startup methodology</li><li>Entrepreneurial motivation</li></ol><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Peter-Cauton">http://www.quora.com/Peter-Cauton</a></span></div></div>'),
(48, 64, 'mio galang', '<br>', 'Software Development ', '', '', '', '', '', '', '<br>', ''),
(50, 66, 'Glenn Santos', 'Founder of Mentors Dojo. Like John Snow, I know nothing.<br><div><br></div><div>Likes:&nbsp;</div><div>Changing the way people think when trying to solve problems</div><div><br></div><div>Dislikes:</div><div>Flying roaches</div>', 'Business Development', 'Startups', 'glennsantos', 'glennsantos', 'theglennsantos', '', '', 'Business models that will work for an education site like this. How to attract mentors to sign up and lend their expertise.<br>', ''),
(52, 68, 'Ari Bancale', '<span style="font-family: arial, sans-serif; text-align: left; ">Game Master. Happiness Coach. Practicing autotelic holodoxy.</span>&nbsp;Meaning,&nbsp;<span style="font-family: arial, sans-serif; text-align: left; ">I share fun and happiness, and I help people do the same. I have a Masters in Technology Management, and I would like to apply positive psychology, behavioral economics, and human factors in creating games.&nbsp;</span>&nbsp;<br>', 'Creative, Management', 'Education, Usability, Games', 'http://ph.linkedin.com/in/gamemaster', 'http://facebook.com/gamemaster2.0', '', '', '', 'Focusing on our first quick win. Connecting with game developers for Facebook and mobile platforms.&nbsp;', ''),
(54, 70, 'Christian Blanquera', '<div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Co-founder, <a href="http://openovate.com/" title="" target="_blank">Openovate Labs</a></span></div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br></span></div>I''m a DC native, got my chops in San Francisco and have been developing professionally since 2003 now based in the Philippines. My focus is in PHP, JavaScript, XHTML and CSS. Lately I have been building web apps from server to browser.</span><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Through out my career I''ve worked with Zend, Symfony, Drupal, Magento, ModX, Zen Cart, X Cart, Joomla, CakePHP, Code Igniter, jQuery, Prototype JS, YUI, Dojo, MooTools, ExtJS and many social APIs.</span><br><div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br></span></div><div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place: Openovate Labs</div></span></div>', 'Software Development ', 'PHP Development, Web Applications', 'http://ph.linkedin.com/pub/christian-blanquera/8/24b/978', 'christian.blanquera', 'cblanquera', '', '', '', 'I can mentor on the following:<div><br></div><div><ol><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Applied knowledge of OOP, MVC and Design Patterns</span></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Applied knowledge of agile, scrum, project cycles of all scopes</span></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">PHP, JavaScript, CSS, XSLT, XHTML, HTML5</span></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">JQuery, YUI, Prototype, ExtJS, Dojo, MooTools</span></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Drupal, Joomla, Zend, Symfony, CakePHP, Code Igniter, ModX, Magento</span></li></ol></div>'),
(55, 71, 'Guyi Shen', '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">I am the founder and head of business development for <a href="http://lobangclub.com" title="" target="_blank">Lobangclub</a>. I founded lobangclub because I was really frustrated with not being able to find the prices of stuff I want to buy in Singapore.</span><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Now we have over 50,000+ members in Singapore alone and growing explosively.</span><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">You can download our app in the iPhone appstore, just search for "lobangclub".</span><br><div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br></span></div><div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place: Greenbelt</div></span></div>', 'Business Development', 'Ecommerce, User Experience, Retail Industry, Mobile Applications', 'http://sg.linkedin.com/pub/guyi-shen/8/8a9/995', 'guyiguyi', '', '', '', '', 'I can mentor on the following:<div><br></div><div><ol><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Community Management</span><br></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Copywriting</span><br></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">SEM</span><br></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">SEO</span><br></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">User Acquisition</span><br></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Product Management</span><br></li><li><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Sales</span><br></li></ol><div><font color="#000000" face="Arial, Helvetica, Nimbus Sans L, sans-serif"><span style="line-height: 13px;"><span>Quora: <a class="linkclass" href="http://www.quora.com/Guyi-Shen">http://www.quora.com/Guyi-Shen</a></span></span><br></font></div></div>'),
(56, 72, 'Ann Jacobe', 'Founder, <a href="http://www.shoephoric.com/" title="" target="_blank">Shoephoric</a><div>COO, <a href="http://www.strikermobile.com/" title="" target="">Striker Mobile Media</a></div><div><br></div><div><span style="line-height: 13px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; ">I''ve always been interested in technology thus I pursued Electronics and Communications Engineering as well as Information Technology.&nbsp;</span><br></div><div><font color="#000000" face="Arial, Helvetica, Nimbus Sans L, sans-serif"><span style="line-height: 13px;">I run my own IT companies now which are involved in web and mobile applications development. Recently, my search for physical activities made me discover DANCE and fell in love with TANGO, the Argentine kind.&nbsp;</span></font></div><div><span style="line-height: 13px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; ">I wanted to be good at it and I know that the fastest way is to cross-train to develop my core so I fell in love with exercises that involve MIND AND BODY AWARENESS like Pilates, Gyrotonic, Meridian Stretching and now I''m into Ashtanga and Iyengar Yoga.&nbsp;</span><br></div><div><font color="#000000" face="Arial, Helvetica, Nimbus Sans L, sans-serif"><span style="line-height: 13px;">I love beautiful things and I love FASHION. I am deeply enamored and have a serious obsession with SHOES - the SOLE reason why I created an ONLINE SHOE ORGANIZER which also serves as a social network to all shoe lovers like me.&nbsp;</span><br></font></div><div><font color="#000000" face="Arial, Helvetica, Nimbus Sans L, sans-serif"><span style="line-height: 13px;"><br></span></font></div><div><div style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 14px; ">Mentoring Hours:&nbsp;</div><div style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 14px; ">Preferred Meeting Place:&nbsp;</div></div>', 'Business Development', 'Fashion, Design, Mobile', 'http://ph.linkedin.com/pub/ann-jacobe/4/31b/b83', 'myshoephoria', 'AnnJacobe', '', '', '', 'I can mentor on the following:<div><br></div><div><ol><li>Community Building</li><li>Business Development</li><li>Design and fashion</li><li>Mobile Applications</li></ol><div><br></div></div>'),
(57, 73, 'Alvin Gendrano', '<div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Board Director,&nbsp;</span><font color="#000000" face="Arial, Helvetica, Nimbus Sans L, sans-serif"><span style="line-height: 14px;"><a href="http://psia.org.ph/" title="" target="_blank">Philippine Software Industry Association</a></span></font></div><div><font color="#000000" face="Arial, Helvetica, Nimbus Sans L, sans-serif">Director, Developer and Platform Group, <a href="http://www.microsoft.com/en-ph/default.aspx" title="" target="_blank">Microsoft Philippines</a><br></font></div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br></span></div>"Alvin is a global software industry veteran with wide-ranging business and technology experiences from P&amp;L ownership, business and operations management, business strategy and execution, development, cloud computing, online gaming, and consumer e-commerce. He also ran a global technical services delivery practice with staff in Shanghai, Bangalore, Egypt, and Europe. He is passionate to help grow local software after 15 years competing in the global software industry.&nbsp;</span><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">He is currently Director, Developer and Platform Group for Microsoft Philippines and was recently elected to the Philippine Software Industry Association board. In prior roles, he launched the Xbox Games-on-Demand service, led Business Management for SQLServer, grew an online games marketplace to $300M+/yr, ran global service delivery programs, and is a published author on distributed databases.&nbsp;</span><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; ">Alvin has an MBA from the University of North Carolina with honors, an MS in Computer Science from the University of Arizona (state scholar), and a BSCS from De LaSalle University (cum laude and Best Thesis)."</span><br><div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><br></span></div><div><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; line-height: 15px; "><div>Mentoring Hours:&nbsp;</div><div>Preferred Meeting Place:&nbsp;</div></span></div>', 'Business Development', 'Technical Audience Marketing, Business Development, Strategy, Cloud Computing, Ecommerce', 'http://www.linkedin.com/pub/alvin-gendrano-mba-ms-in-c-s/0/680/515', 'agendrano', '', '', '', '', 'I can mentor on the following:<div><ol><li>Business and operations management</li><li>Business strategy and execution</li><li>Development</li><li>Cloud computing</li><li>Online gaming</li><li>Consumer e-commerce</li></ol></div>'),
(58, 74, 'Terence Pua', '<p class=" description summary" style="margin: 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; word-wrap: break-word; "><font color="#000000" face="Arial, Helvetica, Nimbus Sans L, sans-serif" size="2"><span style="line-height: 16px;">CEO/Co-founder, <a href="http://insynchq.com" title="" target="_blank">Insync</a></span></font></p><p class=" description summary" style="margin: 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; word-wrap: break-word; "><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; font-size: 13px; line-height: 1.2; ">15 years as a software / Internet entrepreneur. Established Philippines subsidiary for Friendster.</span><br></p><p class=" description summary" style="margin: 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; vertical-align: baseline; line-height: 1.2; word-wrap: break-word; color: rgb(0, 0, 0); ">In love with syncing.</p><p class=" description summary" style="margin: 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; word-wrap: break-word; "></p><p class=" description summary" style="margin: 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; word-wrap: break-word; "><font color="#000000" face="Arial, Helvetica, Nimbus Sans L, sans-serif" size="2"><span style="line-height: 16px;">Mentoring Hours: anytime -- send an email at least with a couple of days notice at t@insynchq.com.</span></font></p><p class=" description summary" style="margin: 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; word-wrap: break-word; "><span style="line-height: 16px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; font-size: small; ">Preferred Meeting Place: Insync</span></p><p></p><div><br></div><div id="profile-specialties" style="margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 10px; font-family: Arial, Helvetica, ''Nimbus Sans L'', sans-serif; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 12px; "></div>', 'Business Development,Creative,Finance,Human Resources,Management,Sales and Marketing,Software Development ', 'Product Development, Project Management, Customer Development', 'terencepua', 'terencepua', 'terencepua', '', '', '', 'I can mentor on the following:<div><br></div><div><ol><li>Product Development</li><li>Product Management</li><li>Customer Development</li><li>Ideation</li></ol><div><span>Quora:&nbsp;<a class="linkclass" href="http://www.quora.com/Terence-Pua">http://www.quora.com/Terence-Pua</a></span></div></div>'),
(59, 75, 'Art Ilano', '<span style="font-family: arial, sans-serif; line-height: 18px; text-align: left; ">Teaches marketing, strategic planning and strategic management at the University of the Philippines.<br><br>Consultant to a number of firms including San Miguel Properties Inc., Hyundai Automotive Resources Inc., the Asian Development Bank, Rustan''s Supercenters, and Plantersbank.<br><br>Director at LifeScience Center for Wellness and Preventive Medicine, advising on strategic directions and assisting in business development.<br><br>Director, The PIMA Foundation, a not-for-profit professional consultancy firm.<br><br>Head, Cardinale Consulting, a management consulting and training firm.<br><br>Former Editor-in-Chief of PC Magazine Philippines, SME Insight, Mobile Magazine and currently Managing Editor of Plantersbank''s SME magazine.<br></span>', 'Business Development,Management,Sales and Marketing', 'technology, services, publishing', 'http://ph.linkedin.com/pub/art-ilano/1/332/636', '', 'artilano', '', '', '', 'I can mentor on the following:<div><br></div><div><ol><li>Marketing</li><li>Product Development</li><li>Product Planning</li><li>Business Development<br></li></ol></div>'),
(60, 76, 'Joe Palala', 'yeah its me. im supposed to be a developer but im too busy :|<br>', '', '', '', 'jose.palala', 'jpalala', ' mentorsdojo', 'mentors dojo is a way to collaborate mentors startups and mentees', 'focusing', ''),
(61, 77, 'Ben Adrian Sarmiento', '<br>', 'Software Development ', '', '', 'bensarmiento', '', '', '', '<br>', ''),
(63, 79, 'Jesse Panganiban', 'I''m a startup junkie.<br>', 'Business Development', 'software development', '', 'https://www.facebook.com/jpanganibanph', '', '', '', 'Currently looking for mentors in:<br><br>&nbsp;- Company of Heroes (Playmates as well)<br>&nbsp;- Test Driven Development (Unit Tests, TDD, BDD, Agile, XP, whatever)<br>&nbsp;- Entrepreneurship and Startups<br>&nbsp;- User Experience Design<br><br>Currently working on a consumer startup (in stealth).<br><br>Cheers!<br>', ''),
(67, 83, 'Rachel Jaro', 'Founder of Tripsiders.com<div>PHP Web developer for 6 years</div><div>Advance Drupal Developer for 3 years</div><div><br></div>', 'Software Development ', 'Software, Tourism', 'racheljaro', 'rjaro', '', '', '', 'Marketing, Management, HR, SEO, Finance/Accounting', ''),
(68, 84, 'Carlson Orozco', '<font color="#000000" face="Verdana" size="3">I am a freelance web developer who specializes in PHP, XHTML, CSS, jQuery, Codeigniter, and MySQL. I am a very organized person, and a silent-type worker. I love my work, as far as I love the web scripting. This was really my chosen field, to be a web developer, so I have dedicated my life with my work.<br></font>', 'Software Development ', '', 'carlsonorozco', 'carlsonorozco', 'carlsonorozco', '', '', 'Business Development', ''),
(69, 85, 'Richard Grimaldo', '<br>', 'Creative, Management', 'Digital Publishing', '', 'chard.grimaldo', '', '', '', '<br>', ''),
(70, 86, 'John Louie BiÃ±as', 'testing', 'Business Development, Creative, Finance, Human Resources, Management, Sales and Marketing, Software Development ', 'testing', '', '', '', '', '', 'testing', ''),
(71, 87, 'Test Orozco', '<br>', '', '', '', '', '', '', '', '<br>', ''),
(72, 88, 'jetro barrera', '<br>', 'Software Development ', '', '', '', '', '', '', '<br>', ''),
(73, 89, 'Patricia Anne Alvarez Bayona', '<br>', 'Business Development, Creative', '', '', '', '', '', '', '<br>', ''),
(74, 90, '123', '<br>', '', 'Web development', '', '', '', '', '', '<br>', ''),
(75, 91, 'exesteKayague', '', '', '', 'http://fhtirutp3456.webs.com/apps/blog/show/18835765-affordable-womens-apparel', '', '', '', '', '', ''),
(76, 92, 'Albert Mercado', 'I am a business student with a penchant for learning. Creating has been my hobby ever since and I aspire to do more and become a social entrepreneur by using my knowledge in business, science and technology.', 'Business Development, Creative, Sales and Marketing, Software Development ', 'e-commerce', '', '', '', '', '', 'Online education, medicine industry and a whole lot more.', ''),
(77, 93, 'Mae Isabelle Turiana', 'I<span style="font-size: 12.800000190734863px;">''m Mae, from the Philippines. I''m a Sr. Software Engr. for almost 8 yrs, currently employed under an outsourcing company in US. My undergrad is Computer Science in De La Salle University Manila and I''m currently taking my masters (Masters in Information Systems) in University of the Philippines Open University. My expertise is in Web application development - mainly in J2EE and partly in PHP and CMS such as Joomla. I also tried Android dev''t for 2 weeks so I''m still new with it but I''m very interested to learn more on mobile application development. I hope to learn a lot through this community and be mentored on how to make my ideas into reality and start my own product company.</span>', 'Software Development ', 'Software Development and Services, Concierge', 'mae-isabelle-turiana', 'maeisabelle', '', '', '', '<span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">I would really love to develop applications related to online education, training and professional development. I also would love to be able to use those applications to solve problems in society and also to allow people to collaborate, to share their ideas, and to work together for them to create something relevant, up to date, easily accessible, free/open/cheap learning materials for people all over the world.&nbsp;</span><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">I love to learn about how to have my own product company focus on those kind of applications.</span><br>', ''),
(78, 94, 'mmscsbqmcq', 'vrzcasbl', '', 'vrzcasbl', 'http://www.zjwayfluyi.com/', 'vrzcasbl', '', '', '', 'vrzcasbl', ''),
(79, 95, 'nyfoywswzh', 'dqdbnqqc', '', 'dqdbnqqc', 'http://www.ahtymbkfhz.com/', 'dqdbnqqc', '', '', '', 'dqdbnqqc', ''),
(80, 96, 'uqawmpyntc', 'abzvtxni', '', 'abzvtxni', 'http://www.fqvicarhys.com/', 'abzvtxni', '', '', '', 'abzvtxni', ''),
(81, 97, 'jqbwgrioyp', 'izozgrdy', '', 'izozgrdy', 'http://www.ippyedmnvx.com/', 'izozgrdy', '', '', '', 'izozgrdy', ''),
(82, 98, 'bnntphzimh', 'tyounwox', '', 'tyounwox', 'http://www.asbbqxqhry.com/', 'tyounwox', '', '', '', 'tyounwox', ''),
(83, 99, 'ezcxockwtl', 'qjfnauqu', '', 'qjfnauqu', 'http://www.bsruhhnwej.com/', 'qjfnauqu', '', '', '', 'qjfnauqu', ''),
(84, 100, 'josepalala', 'some thest', 'Human Resources', 'test', '', 'http://', '', '', '', '<br>', ''),
(85, 101, 'deterium73', '<br>', '', '', '', '', '', '', '', '', '<br>'),
(86, 102, '123', 'tfawtawftawft', 'Finance, Human Resources', 'awftawft', '', '', '', '', '', '<br>', '');

-- --------------------------------------------------------

--
-- Table structure for table `member_settings`
--

CREATE TABLE IF NOT EXISTS `member_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) NOT NULL,
  `show_email` int(1) NOT NULL,
  `notify` int(1) NOT NULL,
  `premium` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `member_settings`
--

INSERT INTO `member_settings` (`id`, `mem_id`, `show_email`, `notify`, `premium`) VALUES
(1, 2, 0, 1, 0),
(2, 3, 0, 1, 0),
(3, 4, 0, 1, 0),
(4, 5, 0, 1, 0),
(5, 8, 0, 1, 0),
(6, 9, 0, 1, 0),
(7, 10, 0, 1, 0),
(8, 11, 0, 1, 0),
(9, 12, 0, 1, 0),
(10, 13, 0, 1, 0),
(11, 14, 0, 1, 0),
(12, 15, 0, 1, 0),
(13, 16, 0, 1, 0),
(14, 17, 0, 1, 0),
(15, 18, 0, 1, 0),
(16, 19, 0, 1, 0),
(17, 20, 0, 1, 0),
(18, 21, 0, 1, 0),
(19, 22, 0, 1, 0),
(20, 23, 0, 1, 0),
(21, 24, 0, 1, 0),
(22, 25, 0, 1, 0),
(23, 26, 0, 1, 0),
(24, 27, 0, 1, 0),
(25, 28, 0, 1, 0),
(26, 29, 0, 1, 0),
(27, 30, 0, 1, 0),
(28, 31, 0, 1, 0),
(29, 32, 0, 1, 0),
(30, 33, 0, 1, 0),
(31, 34, 0, 1, 0),
(32, 35, 0, 1, 0),
(33, 36, 0, 1, 0),
(34, 37, 0, 1, 0),
(35, 38, 0, 1, 0),
(36, 39, 0, 1, 0),
(37, 40, 0, 1, 0),
(38, 41, 0, 1, 0),
(39, 42, 0, 1, 0),
(40, 43, 0, 1, 0),
(41, 44, 0, 1, 0),
(42, 45, 0, 1, 0),
(43, 46, 0, 1, 0),
(44, 47, 0, 1, 0),
(45, 48, 0, 1, 0),
(46, 49, 0, 1, 0),
(47, 50, 0, 1, 0),
(48, 51, 0, 1, 0),
(49, 52, 0, 1, 0),
(50, 53, 0, 1, 0),
(51, 54, 0, 1, 0),
(52, 55, 0, 1, 0),
(53, 56, 0, 1, 0),
(54, 57, 0, 1, 0),
(55, 58, 0, 1, 0),
(56, 59, 0, 1, 0),
(57, 60, 0, 1, 0),
(58, 61, 0, 1, 0),
(59, 62, 0, 1, 0),
(60, 63, 0, 1, 0),
(61, 64, 0, 1, 0),
(62, 65, 0, 1, 0),
(63, 66, 0, 1, 0),
(64, 67, 0, 1, 0),
(65, 68, 0, 1, 0),
(66, 69, 0, 1, 0),
(67, 70, 0, 1, 0),
(68, 71, 0, 1, 0),
(69, 72, 0, 1, 0),
(70, 73, 0, 1, 0),
(71, 74, 0, 1, 0),
(72, 75, 0, 1, 0),
(73, 76, 0, 1, 0),
(74, 77, 0, 1, 0),
(75, 78, 0, 1, 0),
(76, 79, 0, 1, 0),
(77, 80, 0, 1, 0),
(78, 81, 0, 1, 0),
(79, 82, 0, 1, 0),
(80, 83, 0, 1, 0),
(81, 84, 0, 1, 0),
(82, 85, 0, 1, 0),
(83, 86, 0, 1, 0),
(84, 87, 0, 1, 0),
(85, 88, 0, 1, 0),
(86, 89, 0, 1, 0),
(87, 90, 0, 1, 0),
(88, 91, 0, 1, 0),
(89, 92, 0, 1, 0),
(90, 93, 0, 1, 0),
(91, 94, 0, 1, 0),
(92, 95, 0, 1, 0),
(93, 96, 0, 1, 0),
(94, 97, 0, 1, 0),
(95, 98, 0, 1, 0),
(96, 99, 0, 1, 0),
(97, 100, 0, 1, 0),
(98, 101, 0, 1, 0),
(99, 102, 0, 1, 0);