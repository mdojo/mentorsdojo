
INSERT INTO `blogposts` (`blogpost_id`, `blogpost_title`, `blogpost_url`, `blogpost_created`, `blogpost_creator`) VALUES
(1, 'Career Talks Blog 1', 'http://blog.mentorsdojo.com/career-talks-march-2014/', '2014-02-22 06:52:45', 'joe'),
(2, 'OpennessPH 3rd Meetup on CityNext and Open Data Philippines Updates', 'http://blog.mentorsdojo.com/opennessph-3/', '2014-04-02 01:20:22', 'joe'),
(3, 'Welcome to the real world, graduating students!', 'http://blog.mentorsdojo.com/welcome-to-the-real-world-graduating-students/', '2014-04-08 08:10:11', 'joe'),
(4, 'Life Tips for Knowledge Workers and Entrepreneurs Part I', 'http://blog.mentorsdojo.com/life-tips-for-knowledge-worker-entreps-part-i/', '2014-04-09 17:57:27', 'joe'),
(5, 'IT JAM 2014', 'http://blog.mentorsdojo.com/it-jam-2014/', '2014-04-11 08:46:06', 'joe'),
(6, 'Mentoring Millenials', 'http://blog.mentorsdojo.com/mentoring-millenials/', '2014-04-15 17:10:06', 'joe'),
(7, 'Why Filipinos are perhaps the best Mentors?', 'http://blog.mentorsdojo.com/why-filipinos-are-perhaps-the-best-mentors/', '2014-04-21 06:35:07', 'joe'),
(8, 'Remote Work Is Awesome - How X-Team does it', 'http://bit.ly/remoteworkisawesome', '2014-04-27 00:00:00', 'joe'),
(9, 'PHPUGPH April Meetup 2014', 'http://blog.mentorsdojo.com/phpug-ph-meetup-april-2014/', '2014-05-04 23:23:00', 'joe');

-- --------------------------------------------------------

--
-- Table structure for table `dojo_article`
--

CREATE TABLE IF NOT EXISTS `dojo_article` (
  `article_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `article_user` int(11) NOT NULL,
  `article_title` text NOT NULL,
  `article_content` text NOT NULL,
  `article_slug` varchar(250) NOT NULL,
  `article_created` datetime NOT NULL,
  `article_tags` text NOT NULL,
  `article_published` datetime NOT NULL,
  `article_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dojo_article`
--

INSERT INTO `dojo_article` (`article_id`, `article_user`, `article_title`, `article_content`, `article_slug`, `article_created`, `article_tags`, `article_published`, `article_modified`) VALUES
(1, 1, 'Basic Introduction to Linux', '<h4>What is Linux?</h4>\r\n\r\n<strong>Linux</strong> is based on an open-source operating system\r\n\r\n<h4>How and When did Linux start?</h4>\r\n\r\n<p>The history of Linux dates back a couple of decades already, to back in the 1970''s. Back then, PC (Personal Computers) were expensive. Imagine computers fitting entire houses, entire buildings.</p> \r\n\r\n<p>Now let''s imagine that computers are just like old mechanical calculators. Mechanical calculators were not electronic - and every calculator manufacturer had their own designs. In the same manner, PC''s in those days were not <a href="http://www.thefreedictionary.com/interoperable">interoperable</a>. </p>\r\n\r\n<p>Each PC maker had their own proprietary differences - a software program were not compatible with other computers - simply because each manufacturer had their own interpretation of how a PC should behave. In short, without a standard way - or a standard system for a computer, systems administrators and users would have to port every software in order for it to work in their own computers. </p>\r\n\r\n<p>Fortunately, Bell Labs developers worked on a solution to make computers <a href="http://www.thefreedictionary.com/interoperable">interoperable</a>. They developed a new operating system that was "1. Simple and elegant, 2. Written in the C programming language instead of assembly code, 3. Able to recycle code."(<a href="http://tldp.org/LDP/intro-linux/html/sect_01_01.html">1</a>). They called their project "UNIX".</p>\r\n\r\n<p>In 1991, Linus Torvalds, who was studying computer science at the University of Helsinki, was working on a freely available, academic version of UNIX. He based his work on a version of UNIX called MINIX, a sort of "build-it-yourself" UNIX. MINIX, is a "Unix-like computer operating system based on a microkernel architecture created by Andrew S. Tanenbaum for educational purposes"(<a href="http://en.wikipedia.org/wiki/MINIX">2</a>). As a side note, microkernel-based system is designed to be flexible and portable across other PC''s.</p>\r\n\r\n<p>This project is what became Linux. Linux today is fully featured UNIX clone, and is used on work stations (another term for desktop computer) and servers. Linux is known on the server-side to be a stable and reliable platform  (which is also powering this website you are reading, BTW).</p>\r\n\r\n<h4>Where can I get Linux?</h4>\r\n<p>The author likes to visit <a href="http://distrowatch.com">Distrowatch</a> to find new Linux distributions. A linux distribution or "distro" is a customized version of Linux. Why are there so  many Linux versions? It is because of the opensource nature of linux that people created different versions of Linux. The reader might be thinking - but wasn''t the goal of linux to make everything interoperable, and standard? True dat! (That is true!) This is also why there are also companies that actually package their own versions of Linux. Examples of these companies are Redhat (a popular commercial server linux distribution), SuSE (now Novell SuSE), and Mandriva (creators of Mandriva Linux), and of course, Canonical (makers of the Debian-based desktop linux system, Ubuntu).  Actually, it is thanks to these opensource-based businesses that Linux has become a viable option for companies to use, without which support would be limited. </p>\r\n\r\n<h4>Why Linux?</h4>\r\n<p>Linux is recommended by software professionals running their own servers. There are many reasons to run a linux machine aside from the fact that it is freely available. It may be popular for servers, but that''s not the only reason. The author would like to point out that just because something is popular and free means it''s a good choice. For one, it is opensource - so the code may be buggy at times but more developers can check it out and improve on it. The other reason is portability. You can install linux on almost any standard Lenovo, Asus, Acer, Toshiba, Sony, DELL, and even on an Apple Mac! </p>\r\n\r\n<h4>How to  download and install linux</h4>\r\n<p>The first step is to decide which version of linux - <a href="https://www.google.com.ph/search?q=Ubuntu">Ubuntu</a>, <a href="https://www.google.com.ph/search?q=Debian">Debian</a>, <a href="<a href="https://www.google.com.ph/search?q=Centos">CentOS</a> and <a href="<a href="https://www.google.com.ph/search?q=Fedora">Fedora</a> are among the top linux distributions.A <a href="https://www.google.com.ph/search?q=how+to+install+linux" title="quick google">quick google</a> should help you find popular tutorials on how to on each of these distros.</p>\r\n\r\n<h4>How to use the terminal (command line)</h4>\r\n<p>Across all linux distributions, these commands (based on UNIX) should work. Here are some basic commands to navigate the file system.\r\n</p>\r\n<table>\r\n    <tr><th>Command</th><th>Description</th><th>How to use</th>\r\n	<tr>\r\n		<td>cd</td><td>Change directory</td><td>cd _path_/_to_/_directory_</td>\r\n	</tr>\r\n   <tr>\r\n		<td>mkdir</td><td>Make directory</td><td><code>mkdir _path_/_to_/_new_directory_</td>\r\n	</tr>\r\n   <tr>\r\n		<td>rmdir</td><td>Remove directory</td><td><code>mkdir _path_/_to_/directory_to_delete</code></td>\r\n	</tr>\r\n\r\n   <tr>\r\n		<td>rm</td><td>Remove files </td><td><code>rm -rf /var/www/mentorsdojo/*</code></td>\r\n	</tr>\r\n</table>\r\n<p>\r\nIf you noticed the last command has an asterisk (*). This is known as an "all" wildcard, meaning it will look at any file with a file name and delete it. Technically, it won''t delete files beginning with a dot (.ssh, for example).</p>\r\n\r\n<p>For ubuntu users, you may <a href="https://help.ubuntu.com/community/UsingTheTerminal">check this out</a> for further information</p>\r\n\r\n<h4>References</h4>\r\n\r\n<ul>\r\n	<li><a href="http://tldp.org/LDP/intro-linux/html/sect_01_01.html">The Linux Documentation Project - History of Linux</a></li>\r\n	<li><a href="http://en.wikipedia.org/wiki/MINIX">Wikipedia - MINIX</a></li>\r\n	<li><a href="http://distrowatch.com">Linux Distribution Watch</a></li>\r\n\r\n</ul>\r\n\r\n<h4>Further Reading</h4>\r\n<ul> \r\n <li><a href="https://help.ubuntu.com/community/Installation">Ubuntu Help - Installation</a></li>\r\n <li><a href="http://lnag.sourceforge.net/">Linux Newbie Administration Guide</a></li>\r\n <li><a href="https://help.ubuntu.com/community/UsingTheTerminal">Ubuntu Help - Using the Terminal</a></li>	\r\n \r\n  <li><a href="http://www.wikihow.com/Install-Ubuntu-Linux">WikiHow - Ubuntu Installation</a></li>\r\n  <li><a href="http://www.unixguide.net/linux/linuxshortcuts.shtml">Linux Shortcuts and Commands</a></li>\r\n\r\n</ul>', 'basic-introduction-to-linux', '2014-05-10 00:54:05', 'linux, intro, basics', '2014-05-10 00:54:05', '2014-05-10 09:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `dojo_confirmkey`
--

CREATE TABLE IF NOT EXISTS `dojo_confirmkey` (
  `ck_userid` int(11) NOT NULL,
  `ck_email` varchar(150) NOT NULL,
  `ck_key` varchar(34) NOT NULL,
  `ck_status` enum('activated','deactivated','expired') NOT NULL,
  `ck_created` datetime NOT NULL,
  `ck_expires` datetime NOT NULL,
  PRIMARY KEY (`ck_userid`),
  UNIQUE KEY `ck_userid` (`ck_userid`),
  UNIQUE KEY `ck_email` (`ck_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dojo_confirmkey`
--

INSERT INTO `dojo_confirmkey` (`ck_userid`, `ck_email`, `ck_key`, `ck_status`, `ck_created`, `ck_expires`) VALUES
(1, 'jose.palala@gmail.com', '1cf08facf36092876b89189a0354ba34', '', '2014-05-10 11:32:07', '2014-05-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dojo_expertise_tags`
--

CREATE TABLE IF NOT EXISTS `dojo_expertise_tags` (
  `expertise_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `expertise_tag` varchar(105) NOT NULL,
  PRIMARY KEY (`expertise_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dojo_highestscore`
--

CREATE TABLE IF NOT EXISTS `dojo_highestscore` (
  `highestscore_id` int(11) NOT NULL AUTO_INCREMENT,
  `highestscore_username` varchar(150) NOT NULL,
  `highestscore_userid` int(11) NOT NULL,
  PRIMARY KEY (`highestscore_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dojo_mentor_expertise`
--

CREATE TABLE IF NOT EXISTS `dojo_mentor_expertise` (
  `me_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `me_mentor` int(11) unsigned NOT NULL,
  `me_expertise` int(11) unsigned NOT NULL,
  PRIMARY KEY (`me_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dojo_topic`
--

CREATE TABLE IF NOT EXISTS `dojo_topic` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_title` varchar(140) NOT NULL,
  `topic_content` text NOT NULL,
  `topic_user` int(11) NOT NULL,
  `topic_expires` datetime NOT NULL,
  `topic_created` datetime NOT NULL,
  `topic_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dojo_topic_post`
--

CREATE TABLE IF NOT EXISTS `dojo_topic_post` (
  `topicpost_id` int(11) NOT NULL AUTO_INCREMENT,
  `topicpost_topic` int(11) DEFAULT NULL,
  `topicpost_user` int(11) DEFAULT NULL,
  `topicpost_title` varchar(140) NOT NULL,
  `topicpost_content` text NOT NULL,
  `topicpost_active` int(11) NOT NULL,
  `topicpost_created` datetime NOT NULL,
  `topicpost_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`topicpost_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dojo_totalpoint`
--

CREATE TABLE IF NOT EXISTS `dojo_totalpoint` (
  `totalpoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `totalpoint_user` int(11) NOT NULL,
  `totalpoint_latest` int(11) NOT NULL,
  `totalpoint_created` datetime NOT NULL,
  PRIMARY KEY (`totalpoint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dojo_userpoint`
--

CREATE TABLE IF NOT EXISTS `dojo_userpoint` (
  `userpoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `userpoint_reward` varchar(100) NOT NULL,
  `userpoint_rewarddesc` text NOT NULL,
  `userpoint_plus` int(5) NOT NULL,
  `userpoint_created` datetime NOT NULL,
  PRIMARY KEY (`userpoint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `jobsid` int(11) NOT NULL AUTO_INCREMENT,
  `jobslink` varchar(255) DEFAULT NULL,
  `jobstitle` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `contactperson` varchar(55) NOT NULL,
  `company` varchar(100) NOT NULL,
  `active` int(1) DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`jobsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


